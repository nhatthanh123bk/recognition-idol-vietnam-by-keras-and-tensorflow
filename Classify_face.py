from Create_Embs import create_embs
import pickle
from sklearn.svm import SVC


image_size = 160
path = "./data/images_croped/"
path_model = "./Model/facenet_keras.h5"


Train_embs,labels_data = create_embs(path,path_model,image_size)
model_train = SVC(kernel='linear', probability=True)
model_train.fit(Train_embs,labels_data)
filename = "./Model/model_classifer/model_svm.sav"
pickle.dump((model_train),open(filename,"wb"))
print("model is saved...")


from sklearn.neighbors import KNeighborsClassifier
model_knn = KNeighborsClassifier(n_neighbors=5)
model_knn.fit(Train_embs,labels_data)
filename = "./Model/model_classifer/model_knn.sav"
print("model_knn is saved...")

