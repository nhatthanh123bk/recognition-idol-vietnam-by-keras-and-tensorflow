import tensorflow as tf 
import cv2
import numpy as np 
import detect_aligned
import os

path1 = "/home/nhatthanh/Desktop/Face_Recognition/data/images/"
path2 = "/home/nhatthanh/Desktop/Face_Recognition/data/images_croped/"

def main():
    detect_face = detect_aligned.detect_and_align()         
    for i in os.listdir(path1):
        if((i in os.listdir(path2) == True)):
            continue
        os.mkdir(path2+str(i))
        for j in os.listdir(path1+str(i)):
            img = cv2.imread(path1 + str(i)+"/"+j,1)
            list_face,_= detect_face.detect_align_face(img)    
            id = 0
            for aligned in list_face:
                cv2.imwrite(path2+str(i)+"/"+str(j)+str(id)+".jpg",aligned)
                id = id + 1

if __name__ == '__main__':
    main()

