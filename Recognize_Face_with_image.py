import numpy as np 
import cv2
import pickle
from keras.models import load_model
from Utils import Normal_data
import argparse
import detect_aligned

labels = {1:"Bang_Kieu",2:"Dam_Vinh_Hung",3:"Ha_Anh_Tuan",4:"Ho_Ngoc_Ha",5:"My_Linh",6:"My_Tam",7:"Noo_Phuoc_Thinh",8:"Son_Tung",9:"Toc_Tien",10:"Tuan_Hung",11:"Tung_Duong",12:"Ung_Hoang_Phuc",13:"Dong_Nhi",14:"Soobin_HS",15:"Issac"}

path_model = "./Model/facenet_keras.h5"
model = load_model(path_model)


def create_emb(image):
	detector = detect_aligned.detect_and_align()
	Input_data,coordinates = detector.detect_align_face(image)
	Input_data = np.asarray(Input_data)
	Input_data = Normal_data.create_input(Input_data,160)
	embs = Normal_data.l2_normalize(model.predict(Input_data))
	return embs,coordinates
def main():
	ap = argparse.ArgumentParser()
	ap.add_argument("-i", "--image",help = "path image!!!")
	args = vars(ap.parse_args())
	img = cv2.imread(args["image"])

	embs,coordinates = create_emb(img)
	model_predict = pickle.load(open("./Model/model_classifer/model_svm.sav","rb"))
	label = model_predict.predict(embs)

	for i in range(embs.shape[0]):
		bb = np.zeros(4)
		bb[0] = coordinates[i][0]
		bb[1] = coordinates[i][1]
		bb[2] = coordinates[i][2]
		bb[3] = coordinates[i][3]
		cv2.rectangle(img, (int(bb[0]),int(bb[1])),(int(bb[2]),int(bb[3])), (0, 255, 0), 1)
		startX = bb[0]
		startY = bb[1] - 15 if bb[1] - 15 > 15 else bb[1] + 15
		print(labels[label[i]])
		cv2.putText(img,labels[label[i]], (int(startX), int(startY)),cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255), 1)
	print(img.shape)
	cv2.imshow("image",img)
	cv2.waitKey(0)    

if __name__ == '__main__':
		main()	
