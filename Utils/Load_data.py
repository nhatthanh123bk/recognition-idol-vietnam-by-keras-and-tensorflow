import os
import cv2
import numpy as np

def load_data(path):
    images_data = []
    labels_data = []
    for i in os.listdir(path):
        for j in os.listdir(path+i):
            img = cv2.imread(path + i + "/"+j,1)
            images_data.append(img)
            labels_data.append(int(i))
    return np.asarray(images_data),np.asarray(labels_data) 
