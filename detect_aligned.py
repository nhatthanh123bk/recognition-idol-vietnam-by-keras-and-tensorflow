import tensorflow as tf 
import cv2
import numpy as np 
from mtcnn.detect_face import create_mtcnn,detect_face
import os

class detect_and_align():
	def __init__(self, margin = 44,minsize = 20,threshold = [ 0.6, 0.7, 0.7 ],factor = 0.709):
		self.margin = margin
		self.minsize = minsize
		self.threshold = threshold
		self.factor = factor
		with tf.Graph().as_default():
			sess = tf.Session()
			with sess.as_default():
				self.pnet, self.rnet, self.onet = create_mtcnn(sess, None)
	
	def detect_align_face(self, image):
		list_aligned = []
		coordinates_face = []
		img_size = np.asarray(image.shape)[0:2]
		bounding_boxes, _ = detect_face(image, self.minsize, self.pnet, self.rnet, self.onet, self.threshold, self.factor)		
		for id in range(len(bounding_boxes)): 
			det = np.squeeze(bounding_boxes[id,0:4])
			bb = np.zeros(4, dtype=np.int32)
			bb[0] = np.maximum(det[0] - self.margin/2, 0)
			bb[1] = np.maximum(det[1] - self.margin/2, 0)
			bb[2] = np.minimum(det[2] + self.margin/2, img_size[1])
			bb[3] = np.minimum(det[3] + self.margin/2, img_size[0])
			
			coordinates = np.zeros(4)
			coordinates[0] = bb[0]
			coordinates[1] = bb[1]
			coordinates[2] = bb[2]
			coordinates[3] = bb[3]
			coordinates_face.append(coordinates)			

			cropped = image[bb[1]:bb[3],bb[0]:bb[2],:]
			aligned = cv2.resize(cropped,(160,160))
			list_aligned.append(aligned)
		return list_aligned,coordinates_face


				
		 
