from keras.models import load_model 
import numpy as np 
import os
import cv2
from Utils import Load_data
from Utils import Normal_data



def create_embs(path,path_model,image_size):
	model = load_model(path_model)
	images_data,labels_data = Load_data.load_data(path)
	Train_data = Normal_data.create_input(images_data,image_size)
	Train_embs = Normal_data.l2_normalize(model.predict(Train_data))
	return Train_embs,labels_data
